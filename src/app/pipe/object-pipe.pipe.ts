import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectPipe'
})
export class ObjectPipePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {

    if (!value) {
      return [];
    }

    return Object.keys(value);
  }

}
