import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeCmpomentRoutingModule } from './employee-cmpoment-routing.module';
import { EmployeeCmpomentComponent } from './employee-cmpoment.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {TableModule} from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TabMenuModule } from 'primeng/tabmenu';
import { ObjectPipePipe } from '../pipe/object-pipe.pipe';

// import { ObjectPipePipe } from './pipe/object-pipe.pipe';

@NgModule({
  declarations: [
    EmployeeCmpomentComponent,
    ObjectPipePipe
  ],
  imports: [
    CommonModule,
    EmployeeCmpomentRoutingModule,
    FormsModule,
    TableModule,
    InputTextModule,
    FormsModule,
    ButtonModule,
    TabMenuModule,
    TableModule,
    ReactiveFormsModule,
  ]
})
export class EmployeeCmpomentModule { }
