import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeCmpomentComponent } from './employee-cmpoment.component';

describe('EmployeeCmpomentComponent', () => {
  let component: EmployeeCmpomentComponent;
  let fixture: ComponentFixture<EmployeeCmpomentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeCmpomentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeCmpomentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
