import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-cmpoment',
  templateUrl: './employee-cmpoment.component.html',
  styleUrls: ['./employee-cmpoment.component.css']
})
export class EmployeeCmpomentComponent implements OnInit {
public resultDepartmentCount = [];
  public candidateData = [{ "id": 11, "name": "Ash", "department": "Finance", "joining_date": '8/10/2016' },
  { "id": 12, "name": "John", "department": "HR", "joining_date": '18/1/2011' },
  { "id": 13, "name": "Zuri", "department": "Operations", "joining_date": '28/11/2019' },
  { "id": 14, "name": "Vish", "department": "Development", "joining_date": '7/7/2017' },
  { "id": 15, "name": "Barry", "department": "Operations", "joining_date": '19/8/2014' },
  { "id": 16, "name": "Ady", "department": "Finance", "joining_date": '5/10/2014' },
  { "id": 17, "name": "Gare", "department": "Development", "joining_date": '6/4/2014' },
  { "id": 18, "name": "Hola", "department": "Development", "joining_date": '8/12/2010' },
  { "id": 19, "name": "Ola", "department": "HR", "joining_date": '7/5/2011' },
  { "id": 20, "name": "Kim", "department": "Finance", "joining_date": '20/10/2010' }];

// 1. Sort by name, joining date.
// 3. Find all candidates with experience more than 2 years.

// 2. Search by name.
// 4. Get distinct departments and count of candidates in each.
// 5. Remove all candidates from 'Development' department.

public cols = [
  { field: 'id', header: 'User Id' },
  { field: 'name', header: 'Name' },
  { field: 'department', header: 'Department' },
  { field: 'joining_date', header: 'Joining Date' },
];

public oldCandidateData = [];
  constructor() { }


  ngOnInit(): void {
    this.candidateData = this.candidateData.length === 0 ? [] : this.sortByNameDate(this.candidateData);
    this.oldCandidateData = JSON.parse(JSON.stringify(this.candidateData));
  }

  sortByNameDate(searchData) {
    searchData.sort((a, b) => {
      const af = a.name;
      const bf = b.name;
      const as = new Date(  this.revertAndReplaceSlashToHyphen(a.joining_date));
      const bs = new Date( this.revertAndReplaceSlashToHyphen(b.joining_date));
        
      if(af == bf) {
          return (as < bs) ? -1 : (as > bs) ? 1 : 0;
      } else {
          return (af < bf) ? -1 : 1;
      }
  });

    return searchData;
  }

  removeDevelopmentCandidate() {
    this.candidateData = this.candidateData.filter((data) => data.department !== "Development");
  }
  
  experienceTwoYears() {
    this.candidateData = this.oldCandidateData.filter((data) => { return (this.ateDifference(new Date( this.revertAndReplaceSlashToHyphen(data.joining_date)))) <= 2 });

  }





  revertAndReplaceSlashToHyphen(date) {
    return date ? date.split("/").reverse().join("-") : '';
  }

  departhmetnCount() {
    this.resultDepartmentCount = this.oldCandidateData.reduce((rerultObj, a) => {
      rerultObj[a.department] = [...rerultObj[a.department] || [], a];
      return rerultObj;
      }, {});
      
  }


  ateDifference(actualDate ) {
    const date1 = actualDate; // the date you already commented/ posted
    const date2: any = new Date(); // today


    const diffInSeconds: number = Math.abs(date2 - date1) / 1000;
    const days: number = Math.floor(diffInSeconds / 60 / 60 / 24);
  //   const hours: number = Math.floor(diffInSeconds / 60 / 60 % 24);
  //   const minutes: number = Math.floor(diffInSeconds / 60 % 60);
  //   const seconds: number = Math.floor(diffInSeconds % 60);
  //   const milliseconds: number = 
  //  Math.round((diffInSeconds - Math.floor(diffInSeconds)) * 1000);

    const months: number = Math.floor(days / 31);
    const years: number = Math.floor(months / 12);
    console.log('years :>> ', years);
    return years;
}

}
