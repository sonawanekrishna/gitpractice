import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Router, ActivatedRoute } from '@angular/router';
import { OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {
    
  }
  
  ngOnInit(): void {
    this.router.navigate([""]);
  }
  title = 'routing-defafult-app';
  activeItem: MenuItem;

  items: MenuItem[] = [
    { label: 'Home', id: '', icon: 'pi pi-fw pi-home' },
    { label: 'Employee', id: 'employee', icon: 'pi pi-fw pi-calendar' },

  ];

  onclick(event) {
    console.log('hey', event);
  }

  onTabchange(item) {
    console.log('hey ', item.activeItem.id);

    this.router.navigate([`/${item.activeItem.id}`]);
  }




}
